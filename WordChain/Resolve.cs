using System.Collections.Generic;

namespace WordChain
{
    public class Resolve
    {
        private readonly IDictionary<string,string[]> _dictionary;

        public Resolve(IDictionary<string, string[]> dictionary)
        {
            _dictionary = dictionary;
        }
        
        /// <summary>
        /// Attempts to resolve word chain from start word to end word
        /// based on the dictionary injected.
        /// </summary>
        /// <param name="start">The start word</param>
        /// <param name="end">The end word</param>
        /// <returns>The word chain or error message</returns>
        public string Action(string start, string end)
        {
            var chain = new Chain { Start = start, End = end };

            if (chain.InvalidInputs(_dictionary))
                return "Please provide valid start and end words of equal length";
            
            chain.Process(_dictionary);

            return chain.ToString();
        }
    }
}