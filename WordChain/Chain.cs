using System;
using System.Collections.Generic;
using System.Linq;

namespace WordChain
{
    public class Chain
    {
        public string Start { get; set; }
        
        public string End { get; set; }

        private string Current { get; set; }

        private List<Chain> Links { get; set; } = new();

        public bool InvalidInputs(IDictionary<string, string[]> dictionary) =>
            string.IsNullOrWhiteSpace(Start) || string.IsNullOrWhiteSpace(End) || Start == End ||
            Start.Length != End.Length || !dictionary.ContainsKey(Start) || !dictionary.ContainsKey(End);
        
        public void Process(IDictionary<string, string[]> dictionary, int count = 0)
        {
            if (count == End.Length)
                return;
            
            if (string.IsNullOrWhiteSpace(Current))
                Current = Start;
            
            var links = (dictionary.ContainsKey(Current) ? dictionary[Current] : Array.Empty<string>())
                .Select(x => new Chain{ Start = Start, End = End, Current = x})
                .Where(x => x.Matches() > count)
                .ToList();
            
            if(!links.Any())
                return;

            Links = links;
            
            Links.ForEach(x => x.Process(dictionary, ++count));
        }

        public override string ToString()
        {
            var finalChain = FinalChain();
            
            return finalChain.Any() ? string.Join(" -> ", finalChain) : $"Current dictionary has no chain between {Start} and {End}";
        }

        private int Matches()
        {
            if (string.IsNullOrWhiteSpace(Current) || Current.Length != End.Length)
                throw new ArgumentException();
            
            var dictionary = new Dictionary<int, char>();
            for (var i = 0; i < End.Length; i++)
            {
                if(End[i] == Current[i])
                    dictionary.Add(i, End[i]);
            }
            
            return dictionary.Count;
        }

        private List<string> FinalChain()
        { 
            var finalChain = new List<string> { Current };
            
            if (Current == End)
                return finalChain;

            var validChain = Links.Where(x => x.FinalChain().Contains(End))
                .OrderBy(x => x.FinalChain().Count).FirstOrDefault();
            
            if (validChain == null) 
                return new List<string>();
            
            finalChain.AddRange(validChain.FinalChain());
            return finalChain;
        }
    }
}