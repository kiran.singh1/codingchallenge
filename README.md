# Coding Challenge: Word Chains

## Prerequisites

Install:
* Dotnet Core 5.0 SDK

## Getting started

The `Setup` project needs to be run to convert the `dictionary.txt` file to a JSON representation of a dictionary with the words as keys and words one letter away as an array value. Any words without any neighbours are left out.

As this takes time the `words.json` file has been included. It is added to the `WordChain.Tests` project for the tests.

### Operation

The library can be used by passing the start and end words to the `Action` method on `Resolve` class, into which the above mentioned dictionary is injected.

The `Action` method will attempt to resolve word chain from the start word to the end word based on the dictionary injected.

Running the tests in `ResolveTests.cs` file in the `WordChain.Tests` project will verify the work done in `WordChain` project.
 
Please run the `Action_ValidWordsInput_ResultAsExpected` set of tests to get an output for the valid cases, as shown below.
More cases can be added by duplicating the `InlineData` line for new start and end words.

![img_1.png](img_1.png)