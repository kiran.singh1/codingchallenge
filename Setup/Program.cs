﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Setup
{
    class Program
    {
        static async Task Main()
        {
            var list = (await File.ReadAllLinesAsync("../../../dictionary.txt"))
                .Select(x => x.Trim().ToLower())
                .Distinct()
                .ToList();
            
            await using (var streamWriter = new StreamWriter(File.OpenWrite("../../../words.json")))
            {
                await streamWriter.WriteLineAsync("{");
                for (var index = 0; index < list.Count; index++)
                {
                    var item = list[index];
                    var oneLetterAway = list.OneLetterAway(item);

                    if (oneLetterAway.Any())
                    {
                        await streamWriter.WriteLineAsync($"\t\"{item}\": [{string.Join(",", oneLetterAway.Select(x => $"\"{x}\""))}],");
                    }
                    
                    Console.WriteLine($"{index + 1} of {list.Count} done");
                }
                await streamWriter.WriteLineAsync("}");
            }

            Console.WriteLine("Done");
        }
    }
}