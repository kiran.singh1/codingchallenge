using System;
using System.Collections.Generic;
using System.Linq;

namespace Setup
{
    public static class ListExtensions
    {
        public static IList<string> OneLetterAway(this IEnumerable<string> list, string word)
        {
            var all = list.Except(new[] { word });
            var items = (from item in all.Where(x => x.Length == word.Length && x.Intersect(word).Count() == word.Length - 1)
                    let index01 = item.IndexOf(item.Except(word).SingleOrDefault())
                    let index02 = word.IndexOf(word.Except(item).SingleOrDefault())
                    where index01 > -1 && index01 == index02 && item.Remove(index01, 1) == word.Remove(index01, 1)
                    select item)
                .ToList();
            Console.WriteLine($"{word}: {string.Join(",", items)}");
            return items;
        }
    }
}