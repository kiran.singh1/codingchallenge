using System.Diagnostics;

namespace WordChain.Tests
{
    public static class StopwatchExtensions
    {
        public static string TimeTaken(this Stopwatch stopwatch) =>
            $"{stopwatch.ElapsedTicks} ticks ({stopwatch.ElapsedMilliseconds} milliseconds)";
    }
}