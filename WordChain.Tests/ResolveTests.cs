using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using FluentAssertions;
using Newtonsoft.Json;
using Xunit;
using Xunit.Abstractions;

namespace WordChain.Tests
{
    public class ResolveTests
    {
        private readonly ITestOutputHelper _testOutputHelper;
        private readonly Resolve _resolve;
        private readonly Stopwatch _stopwatch;
        private const string ErrorMessage = "Please provide valid start and end words of equal length";

        public ResolveTests(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
            _resolve = new Resolve(JsonConvert.DeserializeObject<IDictionary<string, string[]>>(
                File.ReadAllText("../../../../Setup/words.json")));
            _stopwatch = new Stopwatch();
        }

        [Theory]
        [InlineData("cat", "dog")]
        [InlineData("mate", "ours")]
        [InlineData("bicker", "tacked")]
        public void Action_ValidWordsInput_ResultAsExpected(string start, string end)
        {
            // Arrange
            _stopwatch.Start();
            
            // Act
            var actual = _resolve.Action(start, end);

            // Assert
            _stopwatch.Stop();
            actual.Should().NotBeNull();
            _testOutputHelper.WriteLine($"{_stopwatch.TimeTaken()} from '{start}' to '{end}': {actual}");
        }

        [Fact]
        public void Action_WordsWithNoChainPossible_ReturnsNoChainMessage()
        {
            // Arrange
            _stopwatch.Start();
            
            // Act
            var actual = _resolve.Action("cat", "tax");

            // Assert
            actual.Should().Be($"Current dictionary has no chain between cat and tax");
        }

        [Theory]
        [InlineData("dog")]
        [InlineData("")]
        [InlineData("   ")]
        [InlineData(null)]
        [InlineData("xxx")]
        [InlineData("prescient")]
        public void Action_StartInvalid_ReturnsErrorMessage(string invalid)
        {
            // Arrange // Act
            var actual = _resolve.Action(invalid, "dog");

            // Assert
            actual.Should().Be(ErrorMessage);
        }
        
        [Theory]
        [InlineData("cat")]
        [InlineData("")]
        [InlineData("   ")]
        [InlineData(null)]
        [InlineData("xxx")]
        [InlineData("prescient")]
        public void Action_EndNotInDictionary_ReturnsErrorMessage(string invalid)
        {
            // Arrange // Act
            var actual = _resolve.Action("cat", invalid);

            // Assert
            actual.Should().Be(ErrorMessage);
        }
    }
}